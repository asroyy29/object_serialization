using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Mhw : Rpg
    {
        public int totalReview;

        public Mhw(string _date, int _price, string _name, int _totalReview) 
            : base(_date, _price, _name) {
            totalReview = _totalReview;
        }

        public override void Run() {
            Console.WriteLine("Monster Hunter World" + ", total " + name + " " + totalReview);
        }
    }
}