﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Serialization_Deserialization {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine(" Before Serialization");
            Console.WriteLine(" Data");

            Mhw review = new Mhw("09 August 2018", 334999, " review", 133411);
            Bdo playerBase = new Bdo("18 September 2019", 48999, " playerBase", 30000.00);

            OutputObject(review);
            OutputObject(playerBase);
        
            Console.WriteLine("\n Serialization ");

            byte[] reviewByte = Serialize(review);
            byte[] playerBaseByte = Serialize(playerBase);

            Console.WriteLine("\n review: ");
            OutputByte(reviewByte);

            Console.WriteLine("\n playerBase: ");
            OutputByte(playerBaseByte);


            Console.WriteLine("\n Deserialization");
            Console.WriteLine(" After Deserialization");
            Console.WriteLine("\n Data");

            Rpg samereview = Deserialize(reviewByte);
            Rpg sameplayerBase = Deserialize(playerBaseByte);

            OutputObject(samereview);
            OutputObject(sameplayerBase);
        }

        static byte[] Serialize(Object _rpg) {
            BinaryFormatter bf = new BinaryFormatter();
            using (var ms = new MemoryStream()) {
                bf.Serialize(ms, _rpg);
                return ms.ToArray();
            }
        }

        static Rpg Deserialize(byte[] _byteRpg) {
            using (var memStream = new MemoryStream()) {
                var binForm = new BinaryFormatter();
                memStream.Write(_byteRpg, 0, _byteRpg.Length);
                memStream.Seek(0, SeekOrigin.Begin);

                var obj = binForm.Deserialize(memStream);
                return (Rpg)obj;
            }
        }

        static void OutputObject(Rpg _rpg) {
            _rpg.Run();
        }

        static void OutputByte(byte[] _rpgByte) {
            Console.WriteLine("new byte[] { " + string.Join(", ", _rpgByte) + " }");
        }
    }
}
