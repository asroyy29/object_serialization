using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Rpg
    {
        public string date;
        public int price;
        public string name;

        public Rpg(string _date, int _price, string _name) {
            date = _date;
            price = _price;
            name = _name;
        }

        public virtual void Run() {

        }
    }
}