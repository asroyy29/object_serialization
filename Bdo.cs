using System;

namespace Serialization_Deserialization {
    [Serializable]
    public class Bdo : Rpg
    {
        public double playerBase;

        public Bdo(string _date, int _price, string _name, double _playerBase) 
            : base(_date, _price, _name) {
            playerBase = _playerBase;
        }

        public override void Run() {
            Console.WriteLine("Black Desert Online" + ", total " + name + " around " + playerBase);
        }
    }
}